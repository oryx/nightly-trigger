#! /bin/bash
#
# Nightly Trigger
# ===============
#
# Designed for use with Yocto Project repositories managed using git submodules.
#
# Usage:
#   ./nightly-trigger.sh PROJECT_URL BRANCH
#
# Copyright (c) 2019 Beta Five Ltd
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DATESTAMP="`date -Idate`"

if [[ $# == 2 ]]; then
    PROJECT_URL="$1"
    BRANCH="$2"
else
    echo "ERR: Please supply a project URL and a branch name"
    exit 1
fi

# Ensure we have a clean working copy
if [[ ! -e workdir ]]; then
    git clone "${PROJECT_URL}" -b "${BRANCH}" workdir
    cd workdir
else
    cd workdir
    git fetch
    git checkout -B "${BRANCH}" "origin/${BRANCH}"
    git clean -xffd
fi

# Update layers
git submodule update --init --remote

# Commit, tag and push. We allow the commit to fail incase no submodules have
# changed.
git commit -a -m "Nightly snapshot: ${BRANCH} ${DATESTAMP}" || true
git tag --force "nightly/${BRANCH}/${DATESTAMP}"
git push --force origin "nightly/${BRANCH}/${DATESTAMP}"
